import os
import time
import uuid
import pandas as pd
from flask import (flash, Flask, redirect, render_template, request,
                   session, url_for, send_file, jsonify)
from flask_cors import CORS
import logging
from flask_mail import Mail, Message
from pymongo import MongoClient
import openpyxl
import docx
from smtplib import SMTPException
import datetime

secreat_id = uuid.uuid4().hex


# create a flask app instance
app = Flask(__name__)

# Apply cors policy in our app instance
CORS(app)

server_file_name = "server.log"
logging.basicConfig(filename=server_file_name, level=logging.DEBUG)

app.config["SECRET_KEY"] = secreat_id
app.config["expire_plan"] = 1
app.config["back_color"] = "#f2f2f2"
secure_type = "http"

client = MongoClient("mongodb+srv://harshitgadhiya:Hgadhiya8980@codescatter.04ufqjh.mongodb.net/?retryWrites=true&w=majority")

db = client["email_sending_tool"]

def read_docx(file_path):
    try:
        doc = docx.Document(file_path)
        full_text = []

        count = 0
        for para in doc.paragraphs:
            if count==0:
                full_text.append(para.text)
            else:
                full_text.append(para.text)
            count+=1

        return '\n'.join(full_text)

    except Exception as e:
        app.logger.debug(f"Error in get text from document file: {e}")


def read_excel_data(file_path):
    try:
        workbook = openpyxl.load_workbook(file_path)
        sheet = workbook.active

        all_data = []
        count = 0
        for row in sheet.iter_rows(values_only=True):
            if row[0] and row[1] and count != 0:
                all_data.append(list(row))
            count += 1

        return all_data

    except Exception as e:
        app.logger.debug(f"Error in get email data from excel file: {e}")

def register_data(coll_name, new_dict):
    try:
        coll = db[coll_name]
        coll.insert_one(new_dict)

        return "add_data"

    except Exception as e:
        app.logger.debug(f"Error in register data: {e}")

def find_all_cust_details():
    try:
        coll = db["customer_details"]
        res = coll.find({})
        return res

    except Exception as e:
        app.logger.debug(f"Error in find all customer details data: {e}")
        print(e)

def find_all_cust_details_coll(coll_name):
    try:
        coll = db[coll_name]
        res = coll.find({})
        return res

    except Exception as e:
        app.logger.debug(f"Error all data function: {e}")
        print(e)

def sent_mail_gmail(sender_username,host, port, sender_pwd, receiver_email,subject,message_data,attachment_all_file, sender_email, domain_name):
    try:
        subject = subject.replace("{{domain_name}}", domain_name)
        app.config['MAIL_SERVER'] = host
        app.config['MAIL_PORT'] = port
        app.config['MAIL_USE_SSL'] = True
        app.config['MAIL_USERNAME'] = sender_username
        app.config['MAIL_PASSWORD'] = sender_pwd
        mail_app = Mail(app)

        msg_data = Message(subject, sender=sender_email,
                      recipients=[receiver_email])
        msg_data.body = message_data

        for file_path in attachment_all_file:
            with open(file_path, 'rb') as f:
                filename = f.name.split("/")[-1]
                msg_data.attach(filename=filename, content_type='application/octet-stream', data=f.read())

        try:
            mail_app.send(msg_data)
            app.logger.debug(f"Email sent successfully!")
            output_file_path = os.path.abspath("log_main.txt")
            current_date_time = datetime.datetime.now()
            with open(output_file_path, "a") as file:
                file.write(str(current_date_time) + ":" + receiver_email + "    :  Success\n")

        except Exception as e:
            output_file_path = os.path.abspath("log_main.txt")
            current_date_time = datetime.datetime.now()
            with open(output_file_path, "a") as file:
                file.write(str(current_date_time) + ":" + receiver_email + "    :  Failure\n")
            app.logger.debug(f"Error in send mail: {e}")

    except Exception as e:
        app.logger.debug(f"Error in send gmail function: {e}")

def find_all_specific_user(coll_name, di):
    try:
        coll = db[coll_name]
        res = coll.find(di)
        return res

    except Exception as e:
        app.logger.debug(f"Error in find specific user function: {e}")
        print(e)

def delete_data(coll_name, di):
    try:
        coll = db[coll_name]
        res = coll.delete_one(di)
        return res

    except Exception as e:
        app.logger.debug(f"Error in delete data function: {e}")
        print(e)

def update_data(coll_name, prev_data, update_data):
    try:
        coll = db[coll_name]
        coll.update_one(prev_data, update_data)
        return "updated"

    except Exception as e:
        app.logger.debug(f"Error in update data function: {e}")
        print(e)

def read_csv_data(file_path):
    try:
        df = pd.read_csv(file_path, header=None, skiprows=1, names=["name", "domain_name", "email"])
        all_data = []
        for a, b, c in zip(df["name"],df["domain_name"], df["email"]):
            all_data.append([a, b, c])

        return all_data

    except Exception as e:
        app.logger.debug(f"Error in get email data from csv file: {e}")

@app.route("/", methods=["GET", "POST"])
def login_main():
    try:
        return render_template("auth/login.html")

    except Exception as e:
        app.logger.debug(f"Error in login route: {e}")
        return redirect(url_for('login_main', _external=True, _scheme=secure_type))

@app.route("/login", methods=["GET", "POST"])
def login():
    try:
        if request.method=="POST":
            username = request.form["username"]
            apikey = request.form["pwd"]
            db = client["email_sending_tool"]
            coll = db["customer_details"]
            all_response = coll.find({})
            li = []
            for a in all_response:
                if username==a["username"] and apikey == a["api_key"]:
                    app.config["role"] = a["role"]
                li.append([a["username"], a["api_key"]])

            if [username, apikey] in li:
                flash("login successfully......")
                return redirect(url_for('home', _external=True, _scheme=secure_type))
            else:
                flash("Please get your subscription......")
                return render_template("auth/login.html")
        else:
            return render_template("login.html")

    except Exception as e:
        app.logger.debug(f"Error in login route: {e}")
        return redirect(url_for('login_main', _external=True, _scheme=secure_type))

# That is logout route and clear the current session
@app.route('/logout', methods=['GET', 'POST'])
def logout():
    """
    That funcation was logout session and clear user session
    """

    try:
        session.clear()
        return redirect(url_for('login_main', _external=True, _scheme=secure_type))

    except Exception as e:
        app.logger.debug(f"Error in logout route: {e}")
        return redirect(url_for('login_main', _external=True, _scheme=secure_type))

@app.route("/home", methods=["GET", "POST"])
def home():
    try:
        color = app.config["back_color"]
        session["color"] = color
        role = app.config.get("role", "")
        if role and role=="admin":
            role_main = role
        else:
            role_main=""

        return render_template("index.html", role_main=role_main)

    except Exception as e:
        app.logger.debug(f"Error in home route: {e}")
        return redirect(url_for('login_main', _external=True, _scheme=secure_type))

@app.route("/view_logs", methods=['GET'])
def view_logs():
    try:
        file = os.path.abspath("log_main.txt")
        lines = []
        with open(file, "r") as f:
            lines += f.readlines()
        return render_template("logs.html", lines=lines)

    except Exception as e:
        print(e)

@app.route("/download_logs", methods=['GET'])
def download_logs():
    try:
        server_file_name = "log_main.txt"
        file = os.path.abspath(server_file_name)
        return send_file(file, as_attachment=True)

    except Exception as e:
        print(e)

@app.route("/view_server_logs", methods=['GET'])
def view_server_logs():
    try:
        server_file_name = "server.log"
        file = os.path.abspath(server_file_name)
        lines = []
        with open(file, "r") as f:
            lines += f.readlines()
        return render_template("logs.html", lines=lines)

    except Exception as e:
        app.logger.debug(f"Error in show log api route : {e}")
        return {"message": "data is not present"}


@app.route("/process", methods=["GET", "POST"])
def process():
    try:
        color = app.config["back_color"]
        session["color"] = color
        role = app.config.get("role", "")
        if role and role == "admin":
            role_main = role
        else:
            role_main = ""
        if request.method == "POST":
            username = request.form.get("username", "none")
            password = request.form.get("password", "none")
            host = request.form.get("host", "none")
            sender_email = request.form.get("sender_email", "none")
            port = request.form.get("port", "none")
            subject = request.form.get("subject", "none")
            if not username or username=="none":
                username = "AKIAUXX23TRRO76KLX7F"
            if not password or password=="none":
                password = "BBplXSumOosGq3yyTGEJprIOpfqpChSCBLo2LCku4qpR"
            if not host or host=="none":
                host = "email-smtp.ap-south-1.amazonaws.com"
            if not port or port=="none":
                port = 465
            if not sender_email or sender_email=="none":
                sender_email = "sana@webapphealing.com"


            message_file = request.files['message_file']
            mail_file = request.files['mail_file']
            attachment_file = request.files.getlist('attachment_file')
            msg_save_dir = os.path.abspath("static/uploaded/msg_file")
            send_save_dir = os.path.abspath("static/uploaded/send_file")
            attach_save_dir = os.path.abspath("static/uploaded/attach_file")

            send_name = mail_file.filename
            exten = send_name.split(".")[-1]
            sp_data_path = os.path.join(send_save_dir, mail_file.filename)
            if send_name.split(".")[-1] == "xlsx" or send_name.split(".")[-1] == "csv":
                mail_file.save(sp_data_path)
            else:
                flash("mail sending file not supported. only csv and excel file supported......")
                return redirect(url_for('home', _external=True, _scheme=secure_type))

            msg_data_path = os.path.join(msg_save_dir, message_file.filename)
            message_file.save(msg_data_path)
            msg_name = message_file.filename
            msg_exten = msg_name.split(".")[-1]

            attachment_all_file = []
            for file in attachment_file:
                if file.filename != "":
                    attach_file_path = os.path.join(attach_save_dir, file.filename)
                    attachment_all_file.append(attach_file_path)
                    file.save(attach_file_path)

            if port:
                port = int(port)
            if exten == "xlsx":
                all_data = read_excel_data(sp_data_path)
            else:
                all_data = read_csv_data(sp_data_path)
            for data_one in all_data:
                em, cus_name, domain_name = data_one[2], data_one[0], data_one[1]
                body_text = ""
                line_count = 0
                if msg_exten == "txt":
                    with open(msg_data_path, 'r', encoding="latin-1") as f:
                        for line in f:
                            if line_count == 0:
                                body_text1 = line.replace(",\n", "")
                                body_text = body_text + str(body_text1) + " " + str(cus_name) + ",\n"
                            else:
                                if line:
                                    new_line = line.replace("Â", "")
                                    body_text = body_text + str(new_line)
                                else:
                                    new_line = line.replace("Â", "")
                                    body_text = body_text + str(new_line)
                            line_count += 1
                else:
                    body_text = read_docx(msg_data_path)
                body_text_2 = body_text.replace("{{name}}", cus_name)
                body_text = body_text_2.replace("{{domain_name}}", domain_name)
                register_data(coll_name="emails_data", new_dict={"emall": em, "subject": subject, "mailtext": body_text})
                sent_mail_gmail(username, host, port, password, em, subject, body_text, attachment_all_file,
                              sender_email, domain_name)
                time.sleep(2)
            flash("Email sent successfully......")

            return redirect(url_for('home', _external=True, _scheme=secure_type))
        else:
            return render_template("index.html", role_main=role_main)

    except Exception as e:
        app.logger.debug(f"Error in process route: {e}")
        return redirect(url_for('home', _external=True, _scheme=secure_type))


@app.route("/admin_panel", methods=["GET", "POST"])
def admin_panel():
    try:
        return render_template("home.html")
    except Exception as e:
        print(e)
        return redirect(url_for('admin_panel', _external=True, _scheme=secure_type))


@app.route("/user_admin_panel", methods=["GET", "POST"])
def user_admin_panel():
    try:
        return render_template("user_admin_panel.html")
    except Exception as e:
        print(e)
        return redirect(url_for('user_admin_panel', _external=True, _scheme=secure_type))


@app.route("/color_change_admin", methods=["GET", "POST"])
def color_change_admin():
    try:
        if request.method == "POST":
            color = request.form["color"]
            app.config["back_color"] = color
            return render_template("color_changes.html")
        else:
            return render_template("color_changes.html")

    except Exception as e:
        print(e)
        return redirect(url_for('color_change_admin', _external=True, _scheme=secure_type))


@app.route("/color_change_user", methods=["GET", "POST"])
def color_change_user():
    try:
        if request.method == "POST":
            color = request.form["color"]
            app.config["back_color"] = color
            return render_template("color_changes_admin.html")
        else:
            return render_template("color_changes_admin.html")

    except Exception as e:
        print(e)
        return redirect(url_for('color_change_user', _external=True, _scheme=secure_type))


@app.route("/background_change_admin", methods=["GET", "POST"])
def background_change_admin():
    try:
        if request.method == "POST":
            file = request.files['resume_link']  # Access the uploaded file using the 'file' key
            file_path = os.path.join("static/", "pexels-photo-1591062.jpeg")  # Create the full file path

            if os.path.exists(file_path):  # Check if the file exists
                os.remove(file_path)  # Delete the file

            file_path1 = os.path.join("static/", "pexels-photo-1591062.jpeg")  # Create the full file path
            file.save(file_path1)
            return render_template("background_changes.html")

        else:
            return render_template("background_changes.html")
    except Exception as e:
        print(e)
        return redirect(url_for('background_change_admin', _external=True, _scheme=secure_type))


@app.route("/background_change_user", methods=["GET", "POST"])
def background_change_user():
    try:
        if request.method == "POST":
            file = request.files['resume_link']  # Access the uploaded file using the 'file' key
            file_path = os.path.join("static/", "pexels-photo-1591062.jpeg")  # Create the full file path

            if os.path.exists(file_path):  # Check if the file exists
                os.remove(file_path)  # Delete the file

            file_path1 = os.path.join("static/", "pexels-photo-1591062.jpeg")  # Create the full file path
            file.save(file_path1)
            return render_template("background_changes_admin.html")

        else:
            return render_template("background_changes_admin.html")
    except Exception as e:
        print(e)
        return redirect(url_for('background_change_user', _external=True, _scheme=secure_type))


@app.route("/logo_change_admin", methods=["GET", "POST"])
def logo_change_admin():
    try:
        if request.method == "POST":
            file = request.files['resume_link']  # Access the uploaded file using the 'file' key
            file_path = os.path.join("static/", "main_logo.png")  # Create the full file path

            if os.path.exists(file_path):  # Check if the file exists
                os.remove(file_path)  # Delete the file

            file_path1 = os.path.join("static/", "main_logo.png")  # Create the full file path
            file.save(file_path1)
            return render_template("logo_changes.html")
        else:
            return render_template("logo_changes.html")
    except Exception as e:
        print(e)
        return redirect(url_for('logo_change_admin', _external=True, _scheme=secure_type))


@app.route("/logo_change_user", methods=["GET", "POST"])
def logo_change_user():
    try:
        if request.method == "POST":
            file = request.files['resume_link']  # Access the uploaded file using the 'file' key
            file_path = os.path.join("static/", "main_logo.png")  # Create the full file path

            if os.path.exists(file_path):  # Check if the file exists
                os.remove(file_path)  # Delete the file

            file_path1 = os.path.join("static/", "main_logo.png")  # Create the full file path
            file.save(file_path1)
            return render_template("logo_changes_admin.html")
        else:
            return render_template("logo_changes_admin.html")
    except Exception as e:
        print(e)
        return redirect(url_for('logo_change_user', _external=True, _scheme=secure_type))


@app.route("/user_panel", methods=["GET", "POST"])
def user_panel():
    try:
        all_response = find_all_cust_details()
        if request.method == "POST":
            username = request.form["username"]
            api_key = request.form["api_key"]
            role = request.form["role"]

            all_response1 = find_all_cust_details()
            li = []
            for a in all_response1:
                li.append(a["username"])

            if username not in li:
                di = {"username": username, "api_key": api_key, "role": role}
                res = register_data(coll_name="customer_details", new_dict=di)
                return render_template("user_panel.html", all_response=all_response)
            else:
                flash("username already exits..")
                return render_template("user_panel.html", all_response=all_response)

        else:
            return render_template("user_panel.html", all_response=all_response)
    except Exception as e:
        print(e)
        return redirect(url_for('user_panel', _external=True, _scheme=secure_type))


@app.route("/deletedata/<id_num>", methods=["GET", "POST"])
def deletedata(id_num):
    try:
        di = {}
        di["api_key"] = id_num
        res = delete_data(coll_name="customer_details", di=di)
        return redirect(url_for('user_panel', _external=True, _scheme=secure_type))

    except Exception as e:
        print(e)
        return redirect(url_for('user_panel', _external=True, _scheme=secure_type))

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)